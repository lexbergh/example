<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\CurrencyService;

class CurrencyImportCommand extends Command
{
    /**
     * Command code
     */
    const COMMAND_CODE = 'directory:currency:import';

    /**
     * Fail response code for command
     */
    const FAIL_RESPONSE_CODE = 0;

    /**
     * Success response code for command
     */
    const SUCCESS_RESPONSE_CODE = 1;

    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * CurrencyImportCommand constructor.
     * @param CurrencyService $currencyService
     */
    public function __construct(CurrencyService $currencyService)
    {
        parent::__construct(self::COMMAND_CODE);
        $this->currencyService = $currencyService;

        $this->setDescription('Import currency rates')
            ->setHelp('This command update currency rates');
    }

    /**
     * @SuppressWarnings("unused")
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $rates = $this->currencyService->importRates();

            $table = (new Table($output))
                ->setHeaders(['From', 'To', 'Old rate', 'Current rate'])
                ->setStyle((new TableStyle())->setPadType(STR_PAD_LEFT));

            foreach ($rates as $rate) {
                $table->addRow([
                    $rate['from'],
                    $rate['to'],
                    $rate['old_rate'],
                    $rate['rate'],
                ]);
            }

            $table->render();

            return self::SUCCESS_RESPONSE_CODE;
        } catch (\Throwable $e) {
            $output->writeln("<error>" . $e->getMessage() . "</error>");
            return self::FAIL_RESPONSE_CODE;
        }
    }
}
