<?php

namespace App\Repository;

use App\Entity\CountryRegion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CountryRegionRepository extends ServiceEntityRepository
{
    /**
     * CountryRegionRepository constructor.
     * @param RegistryInterface $registry
     * @codeCoverageIgnore
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CountryRegion::class);
    }
}
