<?php

namespace App\Repository;

use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CountryRepository extends ServiceEntityRepository
{
    /** @var LoggerInterface  */
    private $logger;

    /**
     * CountryRepository constructor.
     * @param RegistryInterface $registry
     * @param LoggerInterface $logger
     * @codeCoverageIgnore
     */
    public function __construct(RegistryInterface $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Country::class);
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function getCountries(): array
    {
        $this->logger->info('Looking for saved countries');
        $fields = ['p.id', 'p.iso3Code as iso3_code', 'p.phoneCode as phone_code', 'p.name'];
        $query = $this->createQueryBuilder('p')
            ->select($fields)
            ->getQuery();

        return $query->execute();
    }
}
