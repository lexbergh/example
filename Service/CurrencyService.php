<?php

namespace App\Service;

use App\Exceptions\CurrencyException;
use GuzzleHttp\Client as ProviderClient;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\Cache\Simple\FilesystemCache;

class CurrencyService
{
    /**
     * Common currency
     */
    const COMMON_CURRENCY = 'USD';

    /**
     * Currencies
     */
    const CURRENCIES = [
        'AUD', 'CAD', 'EUR', 'GBP', 'USD',
    ];

    /**
     * @var ProviderClient
     */
    private $providerClient;

    /**
     * @var FilesystemCache
     */
    private $cache;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $accessKey;

    /**
     * @var array
     */
    private $rates;

    /**
     * CurrencyService constructor.
     *
     * @param ProviderClient $providerClient
     * @param string $accessKey
     * @param FilesystemCache $cache
     * @param LoggerInterface $logger
     */
    public function __construct(
        ProviderClient $providerClient,
        string $accessKey,
        FilesystemCache $cache,
        LoggerInterface $logger
    ) {
        $this->providerClient = $providerClient;
        $this->accessKey = $accessKey;
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @return array
     * @throws CurrencyException
     */
    public function getRates(): array
    {
        $result = [];

        foreach (self::CURRENCIES as $currencyTo) {
            $result[] = [
                'from' => self::COMMON_CURRENCY,
                'to' => $currencyTo,
                'rate' => $this->getCurrencyRate(self::COMMON_CURRENCY . $currencyTo),
            ];
        }

        return $result;
    }

    /**
     * @return array
     * @throws CurrencyException
     */
    public function importRates(): array
    {
        $this->logger->info('Import rates for currencies: {currencies}', [
            'currencies' => implode(', ', self::CURRENCIES),
        ]);

        $result = [];

        $response = $this->providerClient->get('', [
            'query' => [
                'source' => self::COMMON_CURRENCY,
                'currencies' => implode(',', self::CURRENCIES),
                'access_key' => $this->accessKey,
            ],
        ]);

        $response = json_decode((string)$response->getBody(), true);
        $rates = $response['quotes'] ?? [];

        $this->logger->info('For currencies the exchange rates was find', [
            'rates' => $rates,
        ]);

        if ($diff = array_udiff_assoc(self::CURRENCIES, array_keys($rates), [$this, 'checkCurrency'])) {
            $errorMessage = sprintf('Cannot retrieve rate for %s.', implode(',', $diff));
            $this->logger->error($errorMessage);

            throw new CurrencyException($errorMessage);
        }

        foreach ($rates as $currencyPair => $rate) {
            $result[$currencyPair] = [
                'from' => self::COMMON_CURRENCY,
                'to' => str_replace(self::COMMON_CURRENCY, '', $currencyPair),
                'old_rate' => (float)$this->getCacheValue($currencyPair),
                'rate' => $rate,
            ];

            $this->setCacheValue($currencyPair, $rate);

            $this->logger->info('For currency pair {currencyPair} the exchange rate was updated to {rate}', [
                'currencyPair' => $currencyPair,
                'rate' => $rate,
            ]);
        }

        return $result;
    }

    /**
     * @param string $selfCurrency
     * @param string $responseCurrency
     *
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function checkCurrency(string $selfCurrency, string $responseCurrency): bool
    {
        return self::COMMON_CURRENCY . $selfCurrency !== $responseCurrency;
    }

    /**
     * @param string $currencyPair
     *
     * @return float
     * @throws CurrencyException
     */
    private function getCurrencyRate(string $currencyPair): float
    {
        $rate = $this->getCacheValue($currencyPair);
        if (!$rate) {
            $this->logger->info('For currency pair {currencyPair} the exchange rate was not find in cache', [
                'currencyPair' => $currencyPair,
            ]);
            if ($this->rates === null) {
                $this->rates = $this->importRates();
            }

            return $this->rates[$currencyPair]['rate'];
        }

        return $rate;
    }

    /**
     * @param string $key
     *
     * @return mixed|null
     * @throws CurrencyException
     */
    private function getCacheValue(string $key)
    {
        try {
            return $this->cache->get($key, 0);
        } catch (InvalidArgumentException $e) {
            $this->logger->error('In process getting value from cache there was error: ' . $e->getMessage());
            throw new CurrencyException($e->getMessage());
        }
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @throws CurrencyException
     */
    private function setCacheValue(string $key, string $value)
    {
        try {
            $this->cache->set($key, $value);
        } catch (InvalidArgumentException $e) {
            $this->logger->error('In process saving value to cache there was error: ' . $e->getMessage());
            throw new CurrencyException($e->getMessage());
        }
    }
}
