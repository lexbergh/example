<?php

namespace App\Controller;

use App\Entity\Country;
use App\Repository\CountryRepository;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CountryController
 * @Route("/country")
 */
class CountryController extends Controller
{
    /**
     * Returns list of countries
     *
     * @Route("", methods={"GET"}, name="countries_get")
     * @SWG\Get(
     *     tags={"Country"},
     *     @SWG\Swagger(
     *         @SWG\Definition(
     *             definition="CountryResponse",
     *             required={"id", "iso3_code", "phone_code", "name"},
     *             @SWG\Property(property="id", type="string"),
     *             @SWG\Property(property="iso3_code", type="string"),
     *             @SWG\Property(property="phone_code", type="string"),
     *             @SWG\Property(property="name", type="string")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Countries, if not found - empty array",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CountryResponse")
     *         )
     *     )
     * )
     *
     * @param CountryRepository $repository
     * @return JsonResponse
     */
    public function getCountries(CountryRepository $repository): JsonResponse
    {
        return $this->json($repository->getCountries());
    }

    /**
     * Returns list of regions in the country
     *
     * @Route("/{id}/region", methods={"GET"}, name="regions_get")
     * @SWG\Get(
     *     tags={"Country"},
     *     @SWG\Swagger(
     *         @SWG\Definition(
     *             definition="CountryRegionsResponse",
     *             required={"id", "code", "name"},
     *             @SWG\Property(property="id", type="integer"),
     *             @SWG\Property(property="code", type="string"),
     *             @SWG\Property(property="name", type="string")
     *         )
     *     ),
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         type="string",
     *         description="Country code: US, FR etc."
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Regions for country, if not found - empty array",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CountryRegionsResponse")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Country not found",
     *     )
     * )
     *
     * @param Country $country
     * @return JsonResponse
     */
    public function getRegionsForCountry(Country $country): JsonResponse
    {
        return $this->json($country->getRegions(), JsonResponse::HTTP_OK, [], ["groups" => ["regionByCountry"]]);
    }
}
