<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;
use VENDOR\AuthBundle\Annotation\CheckAuth;
use App\Exceptions\CurrencyException;
use App\Service\CurrencyService;

/**
 * Class CurrencyController
 * @Route("/currency")
 * @package App\Controller
 */
class CurrencyController extends AbstractController
{
    /**
     * @var CurrencyService
     */
    private $currencyService;

    /**
     * CurrencyController constructor.
     * @param CurrencyService $currencyService
     */
    public function __construct(CurrencyService $currencyService)
    {
        $this->currencyService = $currencyService;
    }

    /**
     * @Route("/rates", methods={"GET"}, name="currency_rates")
     * @SWG\Get(
     *     tags={"Currency"},
     *     @SWG\Swagger(
     *         @SWG\Definition(
     *             definition="CurrencyRate",
     *             required={"from", "to", "rate"},
     *             @SWG\Property(property="from", type="string"),
     *             @SWG\Property(property="to", type="string"),
     *             @SWG\Property(property="rate", type="float")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Currency rates",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CurrencyRate")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Cannot retrieve rates",
     *         @SWG\Schema(type="string")
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function rates(): JsonResponse
    {
        try {
            $response = $this->currencyService->getRates();
            $status = JsonResponse::HTTP_OK;
        } catch (CurrencyException $e) {
            $response = $e->getMessage();
            $status = JsonResponse::HTTP_BAD_REQUEST;
        } catch (\Throwable $e) {
            $response = $e->getMessage();
            $status = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->json($response, $status);
    }

    /**
     * @Route("/import", methods={"GET"}, name="currency_import")
     * @SWG\Get(
     *     tags={"Currency"},
     *     @SWG\Swagger(
     *         @SWG\Definition(
     *             definition="CurrencyImportRate",
     *             required={"from", "to", "old_rate", "rate"},
     *             @SWG\Property(property="from", type="string"),
     *             @SWG\Property(property="to", type="string"),
     *             @SWG\Property(property="old_rate", type="float"),
     *             @SWG\Property(property="rate", type="float")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Import rates",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(ref="#/definitions/CurrencyImportRate")
     *         )
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Cannot import rates",
     *         @SWG\Schema(type="string")
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @SWG\Schema(type="string")
     *     )
     * )
     *
     * @CheckAuth(adminOnly=true)
     * @return JsonResponse
     */
    public function import(): JsonResponse
    {
        try {
            $response = array_values($this->currencyService->importRates());
            $status = JsonResponse::HTTP_OK;
        } catch (CurrencyException $e) {
            $response = $e->getMessage();
            $status = JsonResponse::HTTP_BAD_REQUEST;
        } catch (\Throwable $e) {
            $response = $e->getMessage();
            $status = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->json($response, $status);
    }
}
