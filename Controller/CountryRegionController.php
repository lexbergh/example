<?php

namespace App\Controller;

use App\Entity\CountryRegion;
use App\Repository\CountryRegionRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CountryRegionController
 * @Route("/region")
 */
class CountryRegionController extends Controller
{
    private $regionRepository;

    public function __construct(CountryRegionRepository $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    /**
     * @Route("", methods={"GET"}, name="region_list")
     * @SWG\Get(
     *     tags={"Region"},
     *     summary="Get regions",
     *     description="Get list of regions",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="List of regions",
     *         @SWG\Schema(
     *             type="array",
     *             @Model(type=CountryRegion::class)
     *         )
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Internal error",
     *     )
     * )
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $regions = $this->regionRepository->findAll();
        return $this->json($regions, JsonResponse::HTTP_OK, [], ['groups' => ['region']]);
    }

    /**
     * @Route("/{id}", methods={"GET"}, name="region_show")
     * @SWG\Get(
     *     tags={"Region"},
     *     summary="Get region by id",
     *     description="Get region by id",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Region id",
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Region",
     *         @Model(type=CountryRegion::class)
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Not found",
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Internal error",
     *     )
     * )
     * @param CountryRegion $region
     * @return JsonResponse
     */
    public function getRegion(CountryRegion $region): JsonResponse
    {
        return $this->json($region, JsonResponse::HTTP_OK, [], ['groups' => ['region']]);
    }
}
