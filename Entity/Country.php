<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 */
class Country
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string")
     * @Groups({"region"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="iso3_code", type="string")
     * @Groups({"region"})
     */
    private $iso3Code;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups({"region"})
     */
    private $phoneCode;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups({"region"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="CountryRegion", mappedBy="country")
     * @SWG\Property(
     *     type="array",
     *     @SWG\Items(ref=@Model(type=CountryRegion::class))
     * )
     */
    private $regions;

    public function __construct()
    {
        $this->regions = new ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getIso3Code(): ?string
    {
        return $this->iso3Code;
    }

    /**
     * @param string $iso3Code
     */
    public function setIso3Code(string $iso3Code): void
    {
        $this->iso3Code = $iso3Code;
    }

    /**
     * @return null|string
     */
    public function getPhoneCode(): ?string
    {
        return $this->phoneCode;
    }

    /**
     * @param string $phoneCode
     */
    public function setPhoneCode(string $phoneCode): void
    {
        $this->phoneCode = $phoneCode;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection|CountryRegion[]
     */
    public function getRegions()
    {
        return $this->regions;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'iso3Code' => $this->getIso3Code(),
            'phoneCode' => $this->getPhoneCode(),
            'name' => $this->getName(),
        ];
    }
}
