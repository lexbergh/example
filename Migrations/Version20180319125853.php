<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\Finder\Finder;

/**
 * Class Version20180319125853
 * @package DoctrineMigrations
 */
class Version20180319125853 extends AbstractMigration
{
    const COUNTRY_SQL_FILE = 'country.sql';
    const COUNTRY_REGION_SQL_FILE = 'country_region.sql';

    /**
     * @param Schema $schema
     * @SuppressWarnings("unused")
     */
    public function up(Schema $schema): void
    {
        $this->insertData(self::COUNTRY_SQL_FILE);
        $this->insertData(self::COUNTRY_REGION_SQL_FILE);
    }

    /**
     * @param string $fileName
     */
    private function insertData(string $fileName): void
    {
        $finder = new Finder();
        $finder->in(__DIR__ . '/data/')->name($fileName);

        foreach ($finder as $file) {
            $content = $file->getContents();
            $this->addSql($content);
        }
    }

    /**
     * @param Schema $schema
     * @SuppressWarnings("unused")
     */
    public function down(Schema $schema): void
    {
        $this->addSql(
            'SET FOREIGN_KEY_CHECKS = 0;
                TRUNCATE country;
                TRUNCATE country_region;
                SET FOREIGN_KEY_CHECKS = 1;'
        );
    }
}
