<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Type;

/**
 * Class Version20180315124821
 * @package DoctrineMigrations
 */
class Version20180315124821 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $countryTable = $this->createCountryTable($schema);
        $this->createCountryRegionTable($schema, $countryTable);
    }

    /**
     * @param Schema $schema
     * @return Table
     */
    private function createCountryTable(Schema $schema): Table
    {
        $countryTable = $schema->createTable('country');
        $countryTable->addColumn('id', Type::STRING)->setLength(2)
            ->setComment('Country ISO-2 format');
        $countryTable->addColumn('iso3_code', Type::STRING)->setLength(3)
            ->setComment('Country ISO-3 format');
        $countryTable->addColumn('phone_code', Type::STRING)->setLength(6)
            ->setNotnull(false)->setDefault(null);
        $countryTable->addColumn('name', Type::STRING);
        $countryTable->setPrimaryKey(['id']);

        return $countryTable;
    }

    /**
     * @param Schema $schema
     * @param Table $countryTable
     */
    private function createCountryRegionTable(Schema $schema, Table $countryTable): void
    {
        $countryRegionTable = $schema->createTable('country_region');
        $countryRegionTable->addColumn('id', Type::INTEGER)
            ->setAutoincrement(true)->setUnsigned(true)->setComment('Region Id');
        $countryRegionTable->addColumn('country_id', Type::STRING)->setLength(4)
            ->setNotnull(true)->setComment('Country Id in ISO-2');
        $countryRegionTable->addColumn('code', Type::STRING)->setLength(32)
            ->setComment('Region Code');
        $countryRegionTable->addColumn('name', Type::STRING)->setLength(255)
            ->setNotnull(false)->setDefault(null)->setComment('Region Name');
        $countryRegionTable->setPrimaryKey(['id'])
            ->addIndex(['country_id'], 'IDX_DIRECTORY_COUNTRY_REGION_COUNTRY_ID');
        $countryRegionTable->addForeignKeyConstraint(
            $countryTable,
            ['country_id'],
            ['id'],
            ['onDelete' => 'CASCADE', 'onUpdate' => 'CASCADE']
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $schema->dropTable('country_region');
        $schema->dropTable('country');
    }
}
